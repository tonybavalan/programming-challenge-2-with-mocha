let chai = require("chai");
let chaiHttp = require("chai-http");
let server = require("../app");

chai.should();
chai.use(chaiHttp);


describe("Automated Test Cases", () => {
    describe("/ Initial Route Response", () => {
        it("should return a status code", (done) => {
            chai.request(server)
                .get("/")
                .end((err, res) => {
                    res.should.have.status(200);
                done();
                })
        });
        it("should return a response body of success", (done) => {
            chai.request(server)
                .get("/")
                .end((err, res) => {
                    res.body.should.have.property('success');
                done();
                })
        });
        it("should return a response body of success to be true", (done) => {
            chai.request(server)
                .get("/")
                .end((err, res) => {
                    res.body.success.should.be.eql(true);
                done();
                })
        });
    });
    describe("/report View all the objects", () => {
        it("should return a status code 200 with body of len 4 and the body should be an array", (done) => {
            chai.request(server)
                .get("/report")
                .end((err, res) => {
                    res.should.have.status(200);
                    res.body.should.have.lengthOf(4);
                    res.body.should.be.a('array');
                done();
                })
        });
    });
    describe("/add Add an Object", () => {
        it("should add an object", (done) => {
            const DATA = { StudentName : "Student 5", StudentID : "S5", Subject1 : 85, Subject2 : 80, Subject3 : 92, Subject4 : 100, Subject5 : 98 };
            chai.request(server)
                .post("/add")
                .send(DATA)
                .end((err, res) => {
                    res.should.have.status(201);
                    res.body.should.be.a('object');
                    res.body.should.have.property('StudentName');
                    res.body.should.have.property('StudentID');
                    res.body.should.have.property('Subject1');
                    res.body.should.have.property('Subject2');
                    res.body.should.have.property('Subject3');
                    res.body.should.have.property('Subject4');
                    res.body.should.have.property('Subject5');
                done();
                })
        });
    });
    describe("/update Update an Object", () => {
        it("should return status code 200", (done) => {
            const DATA = { StudentID : "S5", Subject1 : 85 };
            chai.request(server)
                .post("/update")
                .send(DATA)
                .end((err, res) => {
                    res.should.have.status(200);
                done();
                })
        });
        it("should return response as array", (done) => {
            const DATA = { StudentID : "S5", Subject1 : 85 };
            chai.request(server)
                .post("/update")
                .send(DATA)
                .end((err, res) => {
                    res.body.should.be.a('array');
                done();
                })
        });
    });
    describe("/delete Delete an Object", () => {
        it("should return status code 200", (done) => {
            const DATA = { StudentID : "S5" };
            chai.request(server)
                .post("/update")
                .send(DATA)
                .end((err, res) => {
                    res.should.have.status(200);
                done();
                })
        });
        it("should return response as array", (done) => {
            const DATA = { StudentID : "S5" };
            chai.request(server)
                .post("/update")
                .send(DATA)
                .end((err, res) => {
                    res.body.should.be.a('array');
                done();
                })
        });
    });
});