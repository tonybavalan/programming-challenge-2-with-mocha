const express = require('express')
const app = express()
const port = 3000

app.use(express.json());

// Global Variable 
const STUDENTS = [
  { StudentName : "Student 1", StudentID : "S1", Subject1 : 100, Subject2 : 98, Subject3 : 76, Subject4 : 82, Subject5 : 92 },    
  { StudentName : "Student 2", StudentID : "S2", Subject1 : 92, Subject2 : 100, Subject3 : 98, Subject4 : 76, Subject5 : 82 },
  { StudentName : "Student 3", StudentID : "S3", Subject1 : 76, Subject2 : 92, Subject3 : 100, Subject4 : 98, Subject5 : 76 },
  { StudentName : "Student 4", StudentID : "S4", Subject1 : 82, Subject2 : 76, Subject3 : 92, Subject4 : 100, Subject5 : 98 }
]

const MESSAGE = { success: true};

// Initial Route Response
app.get('/', (req, res) => {
  res
    .status(200)
    .send(MESSAGE)
})

// View all the Objects
app.get('/report', async(req, res) => {
  res
    .status(200)
    .send(STUDENTS)
})

// Add an Object
app.post('/add', async(req, res) => {
  const STUDENT = {
    StudentName: req.body.StudentName,
    StudentID: req.body.StudentID,
    Subject1: req.body.Subject1,
    Subject2: req.body.Subject2,
    Subject3: req.body.Subject3,
    Subject4: req.body.Subject4,
    Subject5: req.body.Subject5,
  }
  STUDENTS.push(STUDENT)
  res
    .status(201)
    .send(STUDENT)
});

// Update an Object
app.post('/update', async(req, res) => {
  let request = req.body
  let dataKey = Object.keys(req.body)
  i = STUDENTS.findIndex(obj => obj["StudentID"] == request["StudentID"])
  STUDENTS[i][dataKey[1]] = request[dataKey[1]]
  res
    .status(200)
    .send(STUDENTS)
});

// Delete an Object
app.delete('/delete', async(req, res) => {
  let id = req.body
  d = STUDENTS.findIndex(obj => obj["StudentID"] == id["StudentID"])
  STUDENTS.splice(d,1)
  res
    .status(200)
    .send(STUDENTS)
});

app.listen(port, () => {
  console.log(`app listening at http://localhost:${port}`)
})

module.exports = app;